
module.exports = (sequelize, DataTypes) => {
    const Usuario = sequelize.define('Usuario', {
        nombre: DataTypes.STRING,
        password: DataTypes.STRING,
        dinero: DataTypes.INTEGER,
    }, { tableName: 'usuarios' });
    return Usuario;
};