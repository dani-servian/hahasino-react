const express = require('express');
const router = express.Router();
//requerimos el index.js de models que inicializa sequelize
const model = require('../models/index');
const Usuario = model.Usuario;
const Token = model.Token;
const bcrypt = require ('bcrypt');

// importante: todas las rutas get, post... son relativas a la ruta principal
// de este controlador: /api/pokemons
// GET lista de todos los pokemons
// vinculamos la ruta /api/pokemons a la función declarada
// si todo ok devolveremos un objeto tipo:
// {ok: true, data: [lista_de_objetos_pokemon...]}
// si se produce un error:
// {ok: false, error: mensaje_de_error}


// permitimos acceso desde otro servidor
router.all('/:algo',(req,res,next)=>{
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  next();
});



/* POST registro de usuario */
router.post('/registre', function(req, res, next) {
  // en la bdd siempre guardamos password encriptado
  // el número 10 es el número de "rounds" en el cálculo, cuanto mayor más tarda en computar pero más difícil es descubrir el password
  // https://stackoverflow.com/questions/46693430/what-are-salt-rounds-and-how-are-salts-stored-in-bcrypt
  console.log(req.body.password);
  const hash = bcrypt.hashSync(req.body.password, 10);
  //reemplazamos el password con su versión encriptada
  req.body.password=hash;
  // "create" es un método de sequelize, recibe un objeto con las propiedades/valores de los campos
  let user = {
    nombre: req.body.nombre,
    password: req.body.password,
    dinero: 0,
    createdAt: new Date(),
    updatedAt: new Date()
  };

  model.Usuario.create(user)
  .then((item) => item.save())

  .then((item)=>res.json({ok:true, data:item}))
  .catch((error)=>res.json({ok:false, error:error}))
});


/* GET llista de pokemons */
router.get('/', function (req, res, next) {

  // solo seguimos si existe token válido
  const token_recibido = req.body.token;
  if (!token_recibido) {
    return res.status(400).json({ok:false, error:"token no recibido"});
  }

  //ejemplo de PROTECCION de ruta: si TOKEN no existe, devolvemos error
  model.Token.findOne({ where: { token:token_recibido } })
  .then((token_devuelto)=> {
      if (token_devuelto){
          return  model.Pokemon.findAll();
      } else {
          throw "token incorrecto";
      }
  } )
  .then(pokemons => res.json({
      ok: true,
      data: pokemons
  }))
  .catch((error)=>res.json({ok:false, error:error}));
});



router.get('/', function (req, res, next) {
//findAll es un método de sequelize!
model.Usuario.findAll({ where: {id: req.params.id}}  )
.then(usuarios => res.json({
ok: true,
data: usuarios
}))
.catch(error => res.json({
ok: false,
error: error
}))
});


/* POST LOGIN */
router.post('/login',  (req, res) => {
    //leemos nombre y password del body
    // const { nombre, password } = req.body;
    const nombre = req.body.nombre;
    const password = req.body.password;
    // si nombre / password no se han facilitado devolvemos error con código de estado 400
    if (!nombre || !password) {
      return res.status(400).json({ok:false, error:"nombre o password incorrecto"});
    }
  
    //buscamos usuario y comprobamos si password coincide
    //findOne es un método de sequelize, si no encuentra nada devolverá error
    Usuario.findOne({ where: { nombre: nombre } })
      .then((usuario) => {
        //comparamos el password recibido con el password del usuario guardado en bdd, ambos encriptados
        if (bcrypt.compareSync(password, usuario.password)) {
          //si ok, devolvemos usuario a siguiente "then" 
          return usuario;
        } else {
          // si no coinciden pasamos msg error a "catch"
          throw "password incorrecto";
        }
      })
      .then((usuario)=>{
          //ok, login correcto, creamos un token aleatorio
          let token = '';
          const caractersPossibles = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
          const longitud = 15;
          for (var i = 0; i < longitud; i++) {
            token += caractersPossibles.charAt(
              Math.floor(Math.random() * caractersPossibles.length)
            );
          }
          //devolvemos un nuevo objeto "token" al siguiente then, que incluye id y nombre de usuario
          return Token.create({ token, usuarios_id: usuario.id, nombre_usuario: usuario.nombre })
      })
      .then((token)=>res.json({ok:true, data:token})) //enviamos respuesta con el token completo en json
      .catch((error)=>res.json({ok:false, error:error}));
  
    });



router.delete('/logout',  (req, res) => {
    const token = req.body.token;
    //si no existe el token no aceptamos logout
    if (!token) {
      return res.status(400).json({ok:false, error:"token no recibido"});
    }
    // si lo recibimos, intentamos eliminarlo
    Token.destroy({ where: { token } })
    .then(()=>res.json({ok:true}))
    .catch((error)=>res.json({ok:false, error:error}));

  });

  router.post('/dinero',  (req, res) => {
    const token = req.body.token;
    //si no hi ha token no acceptem
    if (!token) {
      return res.status(400).json({ok:false, error:"dinero no recibido"});
    }
    console.log("estamos en dinero")
    Token.findOne({ where: { token } })
    .then(token => {
      console.log("token recibido", token)
      if (token.usuarios_id){
        return Usuario.findOne({where: {id: token.usuarios_id}});
      }else{
        throw "usuario no valido";
      };
    })
    .then(usuario => res.json({ok:true, data: usuario, dinero: usuario.dinero}))
    .catch((error)=>res.json({ok:false, error:error}));

  });


  router.put('/recargar',  (req, res) => {
    const {token, importe} = req.body;
    //si no hi ha token no acceptem
    if (!token) {
      return res.status(400).json({ok:false, error:"dinero no recibido"});
    }
    console.log("estamos en dinero")
    Token.findOne({ where: { token } })
    .then(token => {
      console.log("token recibido", token)
      if (token.usuarios_id){
        return Usuario.findOne({where: {id: token.usuarios_id}});
      }else{
        throw "usuario no valido";
      };
    })
    .then(usuario => {
      usuario.dinero = usuario.dinero + importe;
      usuario.save();
      return usuario;
    })
    .then(usuario => res.json({ok:true, data: usuario, dinero: usuario.dinero}))
    .catch((error)=>res.json({ok:false, error:error}));

  });




module.exports = router;