import React, { Component } from 'react';
import { BrowserRouter, NavLink, Link, Route, Switch } from 'react-router-dom';
import { Container, Row, Col, Button } from 'reactstrap';
import { CookiesProvider, withCookies } from 'react-cookie';


import Home from './components/Home';
import Saldo from './components/Saldo';
import Login from './components/Login';
import Logout from './components/Logout';
import Register from './components/Register';
import Recarga from './components/Recarga';
import NavMenu from './components/NavMenu';
import Allatu from './componentsAllatu/Allatu.jsx';
import P404 from './components/P404';


import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './app.css';



// https://medium.com/@rossbulat/using-cookies-in-react-redux-and-react-router-4-f5f6079905dc

class App extends Component {
  constructor(props){
    super(props);
    // let pastaUsuarioInicial = (!this.props.cookies.get('dinero')) ? 0 : this.props.cookies.get('dinero');
    this.state = {
    //  dineroUsuario: pastaUsuarioInicial
    pasta: this.props.cookies.get("dinero") 
    }
    this.setPasta = this.setPasta.bind(this);
  }

  setPasta(dinero){
    this.setState({ pasta: dinero});
  }
  // calculaDineroUsuario(premio){
  //   this.setState({
  //     dineroUsuario: premio
  //   })
  //   console.log(this.state.dineroUsuario);
  // }
  render() {
    return (

      <CookiesProvider>
        <BrowserRouter>
        
          <NavMenu pasta={this.state.pasta}/>
          {/* <Container>
            <Row>
              <Col>
                <h1>{this.props.cookies.get('nom')}</h1>
                <h1>{this.props.cookies.get('token')}</h1>
                <h1>{this.props.cookies.get('dinero')}</h1>
              </Col>
            </Row>
          </Container> */}
          <Switch>
                  <Route exact path="/" render={() => <Home cookies={this.props.cookies} />} />
                  <Route path="/login" component={Login} />
                  <Route path="/logout" render={() => (<Logout cookies={this.props.cookies} />)} />
                  <Route path="/register" render={() => (<Register cookies={this.props.cookies} />)} />
                  <Route path="/saldo" render={() => (<Saldo cookies={this.props.cookies} />)} />
                  <Route path="/recarga" render={() => (<Recarga cookies={this.props.cookies} />)} />
                  <Route path="/Allatu" render={() => (<Allatu cookies={this.props.cookies} pasta={this.state.pasta} setPasta={this.setPasta} />)} />
                  <Route path="/Ruleta" render={() => (<Ruleta cookies={this.props.cookies} />)} />
                  <Route component={P404} />
                </Switch>
        </BrowserRouter>
      </CookiesProvider>
    );
  }
}


export default withCookies(App);