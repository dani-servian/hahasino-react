import React from "react";

export default class Slider extends React.Component {
  constructor(props) {
    super(props)    
    
  }

  render() {
    return (
      <div className="d-flex justify-content-center">
        <label className="label-apuesta">
          <input 
            className="input-apuesta"
            id="typeinp" 
            type="range" 
            min="10" max={this.props.cookies.get('dinero')} 
            onChange={this.props.handleChange}
            value={this.props.apuesta} 
            step="1"/><br />
          {this.props.apuesta}€
        </label>
      </div>
    );
  }
}