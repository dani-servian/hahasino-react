import React from 'react';
import './css/cajas.css';

class Caja extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		this.mostrarPrecio = this.mostrarPrecio.bind(this);
	}

	//cambiar state de la clase
	//Imbocamos a la función pasada por props, que viene des del componente Allatu para que cambie la propiedad visible
	//al hacer un onClick a la caja
	//Esto también afectará al componente Premios, ya que, de de la propiedad visible se basará para cambiar el css de
	//una determinada casilla de premios (la que tenga que ver con el id de la caja)
	mostrarPrecio() {
		this.props.destaparCaja(this.props.numCaja);
	}

    render() {
        // Pillamos los valores del id de la caja, su premio, y la propiedad visible
        // con la propiedad visible, comparamos su valor para poner una clase de css u otra, conforme marque una
        // diferencia si ese premio esta ya descartado o aun no
        let idCaja = this.props.numCaja;
        let premio = this.props.premio;
        let display1 = "";
        let display2 = " d-none";
        let showclase = "caja";
        if(this.props.visible === true){
            display1 = " d-none";
            display2 = "";
            showclase = "cajaopen "
        }

        if(this.props.juegoGanado === true){
            display1 = " d-none";
            display2 = "";
            showclase = "cajaopen "
        }
        
        return (
            <div className={showclase+" d-flex justify-content-center"} onClick={this.mostrarPrecio}>
                <div className="num-caja d-flex justify-content-center align-items-center">
                    <span className={"numCaja"+display1}>{idCaja}</span>
                    <span className={"premioCaja"+display2}>{premio}€</span>
                </div>
            </div>
        );
    }
}

export default Caja;
