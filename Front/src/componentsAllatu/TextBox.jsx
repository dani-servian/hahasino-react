import React from "react";
import Contador from './Contador.jsx'
import { Container, Row, Col } from 'reactstrap';

class TextBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}



    }
    render() {
        let numDestapados = this.props.numDestapados;
        let cantidadPremis;

        if (numDestapados < 4) {
            cantidadPremis = 4;
        } else if (numDestapados < 8) {
            cantidadPremis = 8;
        } else if (numDestapados < 11) {
            cantidadPremis = 11;
        } else if (numDestapados === 11) {
            cantidadPremis = numDestapados;
        }


        let cajasPorDestapar = cantidadPremis - numDestapados;


        return (
            <Row>
                <Col />
                <Col xs="6">
                    <div className={this.props.visibility ? "invisible fila-caja" : "visible fila-caja"}>
                        <div className="textBox">

                            <div className="marco-textBox">
                                <div className="text-textBox">
                                    Por favor, abre {cajasPorDestapar} cajas.
                                <Contador apuesta={this.props.apuesta} visibility={this.props.visibility}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </Col>
                <Col />
            </Row >

        );
    }
}

export default TextBox;