import React from "react";
import { Container, Row, Col } from 'reactstrap';

class CajaSeleccionada extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    
    render() {
        let idCaja = this.props.cajaSeleccionada;
        let display1 = "";
        let display2 = " d-none";
        let showclase = "caja ";
        if(this.props.visible === true){
            display1 = " d-none";
            display2 = "";
            showclase = "cajaopen "
        }

        if(this.props.juegoGanado === true){
            display1 = " d-none";
            display2 = "";
            showclase = "cajaopen "
        }
        
        return (
            <div className={this.props.visibility ? "invisible fila-caja" : "visible fila-caja"}>
                <div className="d-flex justify-content-center">
                    <div className={showclase+"d-flex justify-content-center"}>
                        <div className="num-caja d-flex justify-content-center align-items-center">
                            <span className={"numCaja"+display1}>{idCaja}</span>
                            <span className={"premioCaja"+display2}>{this.props.precioActual}€</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CajaSeleccionada;