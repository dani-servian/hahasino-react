import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class Ofertas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };
        this.toggle = this.toggle.bind(this);
    }
    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }
    render() {

        let botonRechazar = <Button color="secondary" className="btn-rechazar" onClick={this.props.toggle}>Rechazar</Button>;
        if (this.props.numDestapados === 11) {
            botonRechazar = <Button color="secondary" onClick={this.props.toggleCajaUser} className="btn-rechazar">Rechazar</Button>;
        }
        return (
            <div>
                <Modal keyboard={false} isOpen={this.props.modalVisible} toggle={this.props.toggle} className={this.props.className} backdrop="static">
                    <ModalHeader>La oferta de la casa es...</ModalHeader>
                    <ModalBody>
                        <h2>{this.props.pasta}€</h2>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" className="btn-aceptar" onClick={this.props.toggleAceptar}>Aceptar oferta</Button>{' '}
                        {botonRechazar}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
export default Ofertas;