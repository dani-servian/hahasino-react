import React from 'react';
import { Button, ButtonGroup, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Slider from './Slider.jsx'

class EscogerCaja extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let premios = this.props.premios;

    let idPremios = []
    for (let i = 0; i < premios.length; i++) {
      idPremios.push(premios[i].id);
    }

    let Buttons = idPremios.map(el => {
      return (
        <Button key={el} color="danger" onClick={() => this.props.generarCajas(el)}>{el}</Button>
      )
    })


    return (
      <div>
        <Modal isOpen={this.props.modal} keyboard={false} toggle={this.props.toggle} className={this.props.className} backdrop="static">
          <ModalHeader>Antes de comenzar...</ModalHeader>
          <ModalBody>
            <h3>¿Cuánto quieres apostar?</h3>            
            <Slider handleChange={this.props.handleChange} apuesta={this.props.apuesta} cookies={this.props.cookies}/>
            <h3>Elige tu caja</h3>
            <ButtonGroup className="botones-escogerCaja">
              {Buttons}
            </ButtonGroup>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default EscogerCaja;