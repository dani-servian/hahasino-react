import React from 'react';

class Contador extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 

         }
    }
    render() {
        let contador = this.props.apuesta; 
        return ( 
            <div className={this.props.visibility ? "invisible contador-apuesta" : "visible contador-apuesta"}>
                Apuesta actual: {contador}€
            </div>
         );
    }
}
 
export default Contador;