import React from "react";
import Caja from './Caja'
import { Container, Row, Col } from 'reactstrap';

class Cajas extends React.Component {
    constructor(props) {
        super(props);
        this.state = { }
    }
    render() {  
        // Metemos un that = this porque a la hora de pasar por props el destaparCaja que viene del componente Allatu,
        // da problemas ya que el this que habia antes hacie referencia al this de dentro del return del map (this.props.premios)
        // Con el that ya hace referencia a otro this mas general (del componente entero)
        let that = this;

        // De state de Allatu, recibimos las propiedades de los objetos de la array y nos deja pasarselas al componente Caja
        // Con destaparCaja, le pasamos la función del componente Allatu al componente Caja, pasandole por parametro el id de una caja
        let cajas = this.props.premios.filter(el => el.id != this.props.cajaSeleccionada).map(function (e) {
            return (
                <Col key={e.id}>
                    <Caja juegoGanado={that.props.juegoGanado} numCaja={e.id} visible={e.visible} premio={e.prize} destaparCaja={() => that.props.destaparCaja(e.id)}/>
                </Col>
            )
        })
        
        return (
            <Container>
                <Row className={this.props.visibility ? "invisible fila-caja" : "visible fila-caja"}>
                    {cajas}
                </Row>
            </Container>
        );
    }
}

export default Cajas;