import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class Aceptar extends React.Component {

    render() {
        
        return (
            <div>
                <Modal keyboard={false} isOpen={this.props.modalAceptar} toggle={this.props.toggleAceptar} backdrop="static">
                    <ModalHeader>¡¡Enhorabuena!!</ModalHeader>
                    <ModalBody>
                        Has ganado: {this.props.pasta} €
                    </ModalBody>
                    <ModalFooter>
                        <a className="btn btn-primary btn-aceptar" onClick={this.props.toggleAceptar} href="/">Volver a jugar</a>{' '}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Aceptar;