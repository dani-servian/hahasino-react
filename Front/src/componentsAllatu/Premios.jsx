import React from "react";
import { Col } from 'reactstrap';
import './css/Premios.css';

class Premios extends React.Component {
    constructor(props) {
        super(props);
        this.state = { }
        
    }
    render() {
        let datos = this.props.premios.map(el => el);
        datos.sort(function(a,b){
            if (a.prize < b.prize){
                return -1;
            }
            if (a.prize > b.prize){
                return 1;
            }
            return 0;
        })
        let premios = datos
        .map(function (el, index=0) {
            let bcpremio = "bc-green";
            if(el.visible === true){
                bcpremio = "bc-red  ";
            }
            index++;
            return (
                <div key={el.id} className={"premio d-flex align-items-center justify-content-center "+bcpremio+"-premio"+index}>{el.prize}€</div>
            )
        });

        return (
            <Col xs="12">
            <div className={this.props.visibility ? "invisible d-flex justify-content-between p-0 premiosBorder" : "visible d-flex justify-content-between p-0 premiosBorder"} >
                {premios}
            </div>
            </Col>
        );
    }
}
export default Premios;