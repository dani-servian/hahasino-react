import React from "react";
import Cajas from './Cajas.jsx';
import Premios from './Premios.jsx';
import Ofertas from './Ofertas.jsx';
import Aceptar from './Aceptar.jsx';
import CajaUser from './Cajauser.jsx';
import EscogerCaja from './EscogerCaja.jsx';
import CajaSeleccionada from './CajaSeleccionada.jsx';
import TextBox from './TextBox.jsx'

import { Row } from 'reactstrap';
// import {BrowserRouter, Switch, Route } from 'react-router-dom';

class Allatu extends React.Component {
    constructor(props) {
        super(props);

        // multiplicadores: variable que contiene los valores por los cuales se multiplicara el dinero de la apuesta
        let multiplers = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1, 1.2, 1.5, 2, 3];

        //array prizes que contiene todos los premios posibles (calculando que el usuario ha apostado 5â‚¬)
        let prizesIni = multiplers.map(el => el * 500);

        // premiosRand: mezclamos la array de prizes para que en las cajas no se sepa que valores tengan
        // Nos va a servir para modificar los prizes del state asociandolos de forma random
        let premiosRandIni = this.shuffleArray(prizesIni);

        // En state hacemos array de premios para asociar un id de caja a un prize (premio) y a una propiedad visible
        // que determinarÃ¡ si la caja se ha destapado, y cambiarÃ¡ de color el premio que haya tocado
        //La propiedad selected nos marcarÃ¡ la caja que haya seleccionado el jugador
        this.state = {
            premios: [
                { id: 1, prize: premiosRandIni[0], visible: false, selected: false },
                { id: 2, prize: premiosRandIni[1], visible: false, selected: false },
                { id: 3, prize: premiosRandIni[2], visible: false, selected: false },
                { id: 4, prize: premiosRandIni[3], visible: false, selected: false },
                { id: 5, prize: premiosRandIni[4], visible: false, selected: false },
                { id: 6, prize: premiosRandIni[5], visible: false, selected: false },
                { id: 7, prize: premiosRandIni[6], visible: false, selected: false },
                { id: 8, prize: premiosRandIni[7], visible: false, selected: false },
                { id: 9, prize: premiosRandIni[8], visible: false, selected: false },
                { id: 10, prize: premiosRandIni[9], visible: false, selected: false },
                { id: 11, prize: premiosRandIni[10], visible: false, selected: false },
                { id: 12, prize: premiosRandIni[11], visible: false, selected: false },
                { id: 13, prize: premiosRandIni[12], visible: false, selected: false },
            ],
            numDestapados: 0,
            modalVisible: false,
            isHidden: true,
            modal: true,
            cajaSeleccionada: 0,
            apuesta: this.props.cookies.get('dinero')/2,
            modalAceptar: false,
            modalCajaUser: false,
            juegoGanado: false, 
            precioActual: 0           
        }
        this.shuffleArray = this.shuffleArray.bind(this);
        this.destaparCaja = this.destaparCaja.bind(this);
        this.generarCajas = this.generarCajas.bind(this);
        this.calcularOferta = this.calcularOferta.bind(this);
        this.toggleCajas = this.toggleCajas.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleAceptar = this.toggleAceptar.bind(this);
        this.toggleCajaUser = this.toggleCajaUser.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    // precioActual: Premio que hay dentro de la caja que seleccionamos en el inicio
    toggleCajaUser() {
        let cajaUsuario = this.state.premios.filter(el => el.id === this.state.cajaSeleccionada)
            .map(el => {
                return { id: el.id, prize: el.prize };
            });
        this.toggleModal();
        this.setState(prevState => ({
            modalCajaUser: !prevState.modalCajaUser,
            precioActual: cajaUsuario[0].prize,
            juegoGanado: true
        }));
    }
    

    // Array que hace un random de los multiplicadores (el como funciona es un misterio)!!!!
    shuffleArray(o) {
        for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    };

    toggleAceptar() {
        let cajaUsuario = this.state.premios.filter(el => el.id === this.state.cajaSeleccionada)
            .map(el => {
                return { id: el.id, prize: el.prize };
            });
            this.toggleModal();
        this.setState(prevState => ({
            precioActual: cajaUsuario[0].prize,
            juegoGanado: true,
            modalAceptar: !prevState.modalAceptar,
            juegoGanado: true
        }));
    }

    generarCajas(id) {
        let numId = id
        this.setState({
            isHidden: false,
            cajaSeleccionada: numId
        }, () => {
            // this.actualizarCajas();
            this.toggleCajas();
        })
    }

    toggleCajas() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    // FunciÃ³n que, al hacer click a una caja (componente Caja), cambia el state de la propiedad visible, y permite
    // ver el premio de la caja en vez del numero, y cambia de color la casilla del premio destapado
    // esta funcion se pasa por props hasta Caja y hasta Premios
    destaparCaja(id) {
        let primeravez = true;
        let premiosNew = this.state.premios.map(el => {
            if (el.id === id) {
                // el.visible = true;
                if (el.visible === true) {
                    primeravez = false;
                } else {
                    el.visible = true;
                }
            }
            return el;
        })
        if (primeravez) {
            this.setState({
                premios: premiosNew,
                numDestapados: this.state.numDestapados + 1
            }, () => {
                if (this.state.numDestapados === 4 || this.state.numDestapados === 8 || this.state.numDestapados === 11) {
                    this.calcularOferta();
                    this.toggleModal();
                }
            });
        }
    }

    // Mete valores de premios en las cajas multiplicando lo apostado por los multiplicadores
    handleChange(event) {
        this.setState({
            apuesta: event.target.value
        }, () => {
            let multiplicadores = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1, 1.2, 1.5, 2, 3];
            let premiosRand = this.shuffleArray(multiplicadores);
            let i = 0;
            let premiosNewPrize = this.state.premios.map(el => {
                el.prize = this.state.apuesta * premiosRand[i];
                i++;
                return el;
            })
                this.setState({
                premios: premiosNewPrize
            })
        }
        );

    }

    toggleModal() {
        this.setState({
            modalVisible: !this.state.modalVisible
        });
    }

    calcularOferta() {
        let premiosDisponibles = this.state.premios.filter(el => el.visible === false).map(el => el.prize);
        let suma = 0;
        premiosDisponibles.forEach(function (element) {
            suma = suma + element;
        })
        let resultado = suma / premiosDisponibles.length;
        let resultadoRound = resultado.toFixed(2);
        this.props.setPasta(resultadoRound);
    }

    render() {
        return (
            <>
                <TextBox apuesta={this.state.apuesta} numDestapados={this.state.numDestapados} premios={this.state.premios} visibility={this.state.isHidden} />                
                <Row>
                    <EscogerCaja handleChange={this.handleChange} apuesta={this.state.apuesta} premios={this.state.premios} modal={this.state.modal} generarCajas={this.generarCajas} toggle={this.toggleCaja} cookies={this.props.cookies}/>
                    {/* le pasamos todo el state entero con nombre premios para asociarle las propiedades de las cajas al elemento cajas */}
                    {/* la funcion destaparCaja se la pasamos a cajas, para que des de ahi se pase al componente caja */}
                    <Cajas juegoGanado={this.state.juegoGanado} premios={this.state.premios} destaparCaja={this.destaparCaja} visibility={this.state.isHidden} cajaSeleccionada={this.state.cajaSeleccionada} />
                </Row>
                {/*Caja seleccionada por el user, la que el jugara*/}
                <CajaSeleccionada juegoGanado={this.state.juegoGanado} precioActual={this.state.precioActual} cajaSeleccionada={this.state.cajaSeleccionada} visibility={this.state.isHidden} />
                <Row>
                    {/* le pasamos todo el state entero con nombre premios para asociarle las propiedades de las cajas a los premios */}
                    <Premios premios={this.state.premios} visibility={this.state.isHidden} />
                </Row>
                <Row>
                    <Ofertas numDestapados={this.state.numDestapados} toggleCajaUser={this.toggleCajaUser} toggleAceptar={this.toggleAceptar} modalVisible={this.state.modalVisible} toggle={this.toggleModal} pasta={this.props.pasta} />
                    <Aceptar modalAceptar={this.state.modalAceptar} toggleAceptar={this.toggleAceptar} pasta={this.props.pasta} />
                    <CajaUser toggleCajaUser={this.toggleCajaUser} precioActual={this.state.precioActual} modalCajaUser={this.state.modalCajaUser}  />
                </Row>
            </>
        );
    }
}
export default Allatu;