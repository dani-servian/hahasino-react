/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class Cajauser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    
    render() {
        // this.props.cookies.set('dinero', this.props.precioActual, { path: '/' });
        // console.log(this.props.cookies.get('dinero'));
        return (
            <div>
                <Modal isOpen={this.props.modalCajaUser} toggle={this.props.toggleCajaUser} className={this.props.className} backdrop="static">
                    <ModalHeader>Tu caja tiene:</ModalHeader>
                    <ModalBody>
                        {this.props.precioActual}€
                        </ModalBody>
                    <ModalFooter>
                        <a className="btn btn-primary btn-rechazar" onClick={this.toggleAceptar} href="/">Ir al Home</a>{' '}
                        <a className="btn btn-primary btn-aceptar" onClick={this.toggleAceptar} href="/Allatu">Volver a jugar</a>{' '}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Cajauser;