import React from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

import imgcasino1 from "./Imagenes/ruleta2.jpg";

export default class CardRuleta extends React.Component {
    render() {
        return (
            <div>
                <Card body inverse color="warning">
                    <CardImg top width="100%" src={imgcasino1} alt="Ruleta" />
                    <CardBody>
                        <CardTitle>¡¡Gira la Ruleta!!</CardTitle>
                        <CardSubtitle>Dale un giro a tu vida.</CardSubtitle>
                        <CardText>Tendrás la oportunidad de multiplicar tus ingresos i/o de ganar un TESLA con tan solo un click.</CardText>
                        <Link to="/Ruleta"><Button >Jugar</Button></Link>
                    </CardBody>
                </Card>
            </div>
        );
    }
};