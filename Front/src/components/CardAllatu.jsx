import React from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

import imgcasino2 from "./Imagenes/allatu.jpg";

export default class CardAllatu extends React.Component {
    render() {
        return (
            <div>
                <Card body inverse color="danger" >
                    <CardImg top width="100%" src={imgcasino2} alt="Allatu" />
                    <CardBody>
                        <CardTitle>¡¡Ha Ha Alla Tú!!</CardTitle>
                        <CardSubtitle>Prueba tu suerte con Alla Tú.</CardSubtitle>
                        <CardText>Destapa cajas para descartar premios hasta abrir el de tu propia caja, o bien acepta nuestras ofertas.</CardText>
                        <Link to="/Allatu"><Button className="amarillo">Jugar</Button></Link>
                    </CardBody>
                </Card>
            </div>
        );
    }
};