import React, { Component } from 'react';
import { API } from './Config';

export default class Saldo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "no hay money",
      usuario: "Usuario invalido"
    }
  }

  componentDidMount() {
    let token = this.props.cookies.get('token');
    console.log("token", token);
    if (token) {
      fetch(API + '/usuarios/dinero', {
        method: 'POST',
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify({ token })
      })
        .then(resp => resp.json())
        .then(resp => {
          console.log(resp)
          if (resp.ok) {
            this.setState({ message: resp.data.dinero });
            this.setState({ usuario: resp.data.nombre });
          }
        })
    }
  }

  render() {
    return (
      <div>
        <h1>Saldo</h1>
        <p>Bienvenido {this.state.usuario} al Casino</p>
        <p>Su saldo es {this.state.message} HAHACoins</p>

      </div>
    );
  }
}