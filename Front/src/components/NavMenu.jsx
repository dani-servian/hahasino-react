import React from 'react';
import { NavLink } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import { withCookies, Cookies } from 'react-cookie';

import imglogo from "./Imagenes/logo.png";

class NavMenu extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    let Acceder = <NavItem key="1">
      <NavLink className="link nav-link" to="/Login">Acceder</NavLink>
    </NavItem>;
    let Salir = <NavItem key="2">
      <NavLink className="link nav-link" to="/Logout">Salir</NavLink>
    </NavItem>;
    let nombreUsuario = <NavItem key="3">
      <NavLink className="link nav-link" to="/">{this.props.cookies.get('nom')}</NavLink>
    </NavItem>;
    let money = <NavItem key="4">
      <NavLink className="link nav-link" to="/">{this.props.pasta}€</NavLink>
    </NavItem>;
    // let token = this.props.cookies.get('token');
    let elementosBarra = [];
    if (!this.props.cookies.get('token')) {
      elementosBarra.push(Acceder);
    } else {
      elementosBarra.push(nombreUsuario);
      elementosBarra.push(money);
      elementosBarra.push(Salir);
    }


    return (
      <div>
        <Navbar color="primary" light expand="md">
          <NavbarBrand href="/"><span>  <img src={imglogo} className="logo-img" alt="logo" />  </span></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink className="link nav-link" to="/">Home</NavLink>
              </NavItem>
              {/* <NavItem>
                <NavLink className="link nav-link" to="/Login">Acceder</NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="link nav-link" to="/Logout">Salir</NavLink>
              </NavItem> */}
              {elementosBarra}

              <UncontrolledDropdown nav inNavbar>


              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

export default withCookies(NavMenu);