import React from 'react';
import CardAllatu from './CardAllatu.jsx';
import CardRuleta from './CardRuleta.jsx';
import Carrousel from './Carrousel';

import './Home.css';

import { Row, Col } from 'reactstrap';

import imgcasino from "./Imagenes/casino.jpg";
//import imgcasino1 from "../Imagenes/ruleta.jpg";



export default class Home extends React.Component {

  render() {

    console.log(this.props.cookies);
    let nombre = this.props.cookies.get('nom');
    console.log(this.props.cookies.get('nom'));
    let cards = "";
    if(!this.props.cookies.get('nom')){
      cards = <Col xs="12">
      <img src={imgcasino} width="100%" alt="Casino" />
    </Col>;
    }else{
      cards = <><Col xs="1"></Col>
              <Col xs="5">
                <CardRuleta />
              </Col>
              <Col xs="5">
                <CardAllatu />
              </Col>
              <Col xs="1"></Col></>;
    }

    return (
      <div className="home-background">
          <Row>
            
            {/* <Col xs="1"></Col>
            <Col xs="5">
              <CardRuleta />
            </Col>
            <Col xs="5">
              <CardAllatu />
            </Col>
            <Col xs="1"></Col> */}
            {cards}
          </Row>
      </div>
    );
  }
}


