import React, { Component } from 'react';
import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselCaption,
} from 'reactstrap';
import './Carrousel.css';

import tesla from "./Imagenes/tesla.jpg";
import ruleta from "./Imagenes/ruleta.jpg";
import cajas from "./Imagenes/cajas.png";
import dados from "./Imagenes/dados.jpg";
import poker from './Imagenes/poker.png';
import texas from './Imagenes/texas.jpg';
import slots from './Imagenes/slots.jpg';
import blackjack from './Imagenes/blackjack.png';

const items = [
    {
        src: tesla,
        altText: 'tesla',
        caption: 'GANA UN TESLA!!!',
    },
    {
        src: ruleta,
        altText: 'ruleta',
        caption: 'RULETA',
    },
    {
        src: cajas,
        altText: 'cajas',
        caption: 'CAJAS DE LA FORTUNA',
    },
    {
        src: dados,
        altText: 'Dados',
        header: 'COMING SOON...',
        caption: 'DADOS',
    },
    {
        src: poker,
        altText: 'Poker',
        header: 'COMING SOON...',
        caption: 'POKER',
    },
    {
        src: texas,
        altText: 'Poker',
        header: 'COMING SOON...',
        caption: 'POKER TEXAS HOLDEM',
    },
    {
        src: slots,
        altText: 'Slots',
        header: 'COMING SOON...',
        caption: 'SLOTS',
    },
    {
        src: blackjack,
        altText: 'Blackjack',
        header: 'COMING SOON...',
        caption: 'BLACKJACK',
    },

];


class Carrousel extends Component {
    constructor(props) {
        super(props);
        this.state = { activeIndex: 0 };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }

    onExiting() {
        this.animating = true;
    }

    onExited() {
        this.animating = false;
    }

    next() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    previous() {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }

    render() {
        const { activeIndex } = this.state;

        const slides = items.map((item) => {
            return (
                <CarouselItem
                    className="carrousel"
                    tag="div"
                    onExiting={this.onExiting}
                    onExited={this.onExited}
                    key={item.src}
                >
                    <img src={item.src} alt={item.altText} />
                    <CarouselCaption
                        className="texto-carrousel"
                        captionText={item.caption}
                        captionHeader={item.header}
                    />
                </CarouselItem>
            );
        });

        return (
            <div>
                <Carousel
                    activeIndex={activeIndex}
                    next={this.next}
                    previous={this.previous}
                >

                    {slides}
                    <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
                    <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
                </Carousel>
            </div>
        );
    }
}


export default Carrousel;