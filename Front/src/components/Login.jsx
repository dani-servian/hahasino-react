import React, { Component } from 'react';
import jajacoindorado from "./Imagenes/jajacoindorado.png";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { withCookies, Cookies } from 'react-cookie';
import { Redirect, Link } from 'react-router-dom';
import { API } from './Config';
import "./Login.css";


class Login extends Component {
  constructor() {
    super();
    //Set default message
    this.state = {
      nombre: '',
      password: '',
      tornar: false
    }
    this.submit = this.submit.bind(this);
    this.canvia = this.canvia.bind(this);
  }

  canvia(event) {
    const v = event.target.value;
    const n = event.target.name;
    this.setState({
      [n]: v
    });
  }

  submit(e) {

    e.preventDefault();
    let n = this.state.nombre;
    let p = this.state.password;
    let data = { nombre: n, password: p };

    fetch(API + '/usuarios/login', {
      method: 'POST',
      headers: new Headers({ 'Content-Type': 'application/json' }),
      body: JSON.stringify(data)
    })
      .then(respuesta => respuesta.json())
      .then(respuesta => {
        if (respuesta.ok === false) {
          throw respuesta.error;
        } else {
          return respuesta.data;
        }
      })
      .then(token => {
        console.log(token);
        if (token) {
          console.log("establint cookies");
          this.props.cookies.set('nom', token.nombre_usuario, { path: '/' });
          this.props.cookies.set('id', token.usuarios_id, { path: '/' });
          this.props.cookies.set('token', token.token, { path: '/' });
          return this.props.cookies.get('token');
        //   this.setState({ tornar: true });
        }
      })
      .then(onlytoken => {
        let objOnlyToken = { token: onlytoken };
        fetch(API + '/usuarios/dinero', {
          method: 'POST',
          headers: new Headers({ 'Content-Type': 'application/json' }),
          body: JSON.stringify(objOnlyToken)
        })
        .then(respuesta => respuesta.json())
        .then(respuesta => {
          if (respuesta.ok === false) {
            throw respuesta.error;
          } else {
            return respuesta.dinero;
          }
        })
        .then(dinero => {
          this.props.cookies.set('dinero', dinero, { path: '/' });
          this.setState({ tornar: true });
          // return dinero;
        })
        .catch(err => console.log(err));
      })
      .catch(err => console.log(err));
  }



  render() {


    if (this.state.tornar === true) {
      return <Redirect to='/' />
    }

    return (
      <>

        <div className="login container h-100">
          <div className="d-flex justify-content-center h-100">
            <div className="user_card">
              <div className="d-flex justify-content-center">
                <div className="brand_logo_container">
                  <img src={jajacoindorado} className="brand_logo" alt="Logo" />
                </div>
              </div>
              <div className="d-flex justify-content-center form_container">
                <form onSubmit={this.submit}>
                  <div className="input-group mb-3">
                    <div className="input-group-append">
                      <span className="input-group-text"><i className="fas fa-user"></i></span>
                    </div>
                    <input type="text" onChange={this.canvia} name="nombre" className="form-control input_user" value={this.state.nombre} placeholder="username" />
                  </div>
                  <div className="input-group mb-2">
                    <div className="input-group-append">
                      <span className="input-group-text"><i className="fas fa-key"></i></span>
                    </div>
                    <input type="password" onChange={this.canvia} name="password" className="form-control input_pass" value={this.state.password} placeholder="password" />
                  </div>

                </form>
              </div>
              <div className="d-flex justify-content-center mt-3 login_container">
                <button onClick={this.submit} type="button" name="button" className="btn login_btn">Login</button>
              </div>
              <div className="mt-4">
                <div className="d-flex justify-content-center links">
                  No tienes una cuenta? <Link to="/register" className="ml-2">Registrate!</Link>
                </div>

              </div>
            </div>
          </div>
        </div>

      </>
    );
  }
}

export default withCookies(Login);